# Remove warning and base import
from __future__ import print_function
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import matplotlib.pyplot as plt
from collections import deque
import math
import random

# Import Numnpy
import numpy as np

# Import Keras
import keras
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Activation, InputLayer, Dropout

# Import environment
from env_1 import env_1
env_1 = env_1()

# Setting up the ANN

out_node = 10
model = Sequential()
model.add(Dense(64, input_shape = (8, ), activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dense(out_node, activation = 'linear'))
model.compile(loss='mse', optimizer='adam', metrics=['mae'])

# Setting up variables
num_episodes = 200000
y = 0.99
eps = 1.0
eps_min = 0.01
decay_factor = 0.999

error = 0.07
error_counter = 0

memory = deque(maxlen=1000)
memory_win = deque (maxlen = 1000)
memory_lose = deque (maxlen = 1000)

success_queue = deque(maxlen = 100)
success_list = []
reward_list = []

# Learning Function
def learn():
    state_list = []
    target_list = []
    sample_batch = []

    batch_size = 64
    batch_size_win = 32
    batch_size_lose = 32

    if(len(memory) < batch_size):
        batch_size = len(memory)

    if(len(memory_win) < batch_size_win):
        batch_size_win = len(memory_win)

    if(len(memory_lose) < batch_size_lose):
        batch_size_lose = len(memory_lose)

    sample_batch.append(random.sample(memory, batch_size))
    sample_batch.append(random.sample(memory_win, batch_size_win))
    sample_batch.append(random.sample(memory_lose, batch_size_lose))

    for i in range(3):
        for state, action, reward, next_state, done in sample_batch[i]:
            target = reward
            if not done:
                target = reward + y * np.amax(model.predict(np.array([next_state])))
            target_f = model.predict(np.array([state]))[0]
            target_f[action] = target
            state_list.append(state)
            target_list.append(target_f)

    model.fit(np.array(state_list), np.array(target_list), epochs=1, verbose=0)



for i in range(num_episodes):

    s = env_1.reset_random()

    done = False
    while not done:

        if np.random.random() < eps:
            a = np.random.randint(0, out_node)
        else:
            a = np.argmax(model.predict(np.array([s])))

        new_s, r, done, moves = env_1.step(a, error)

        if(r == 1):
            memory_win.append([s, a, r, new_s, done])
        elif(r == -1):
            memory_lose.append([s, a, r, new_s, done])
        else:
            memory.append([s, a, r, new_s, done])

        s = new_s

    learn()

    if(eps > eps_min):
        eps *= decay_factor
    else:
        eps = eps_min
    
    success_queue.append(r)
    success = int(success_queue.count(1)/(len(success_queue)+0.0)*100)
    success_list.append(success)

    reward_list.append(r)

    error_counter += 1
    if(error_counter > 1000):
        if (np.mean(success_list[i-100:i]) > 85 and error > 0.03):
            error_counter = 0
            error -= 0.02

    print("Generation " + str(i) + ", Reward: " + str(r) + ", Success: " + str(success) + ", Eps: " + str(eps) + ", Error: " + str(error))

    if(i % 100 == 0):
        np.savetxt("success_dec.txt", success_list, fmt='%3i')
        np.savetxt("reward_dec.txt", reward_list, fmt='%3i')

    if(success > 90):
        model.save("backup_dec_" + str(success ) + ".h5")